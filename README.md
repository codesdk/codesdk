Welcome to codesdk
================
One of the guiding missions of codesdk is to develop in the open. 

To this end, we upload videos of our coding sessions to our youtube channel and also publish all our code to public repos. 
